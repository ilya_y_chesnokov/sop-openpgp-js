const openpgp = require('openpgp');
const fs = require('fs');
const process = require('process');

const CANNOT_DECRYPT = 29;

const decrypt = async (withPassword, sessionKeyOut, withSessionKey, verifyWith, verifyOut, certfile) => {
  const encrypted = fs.readFileSync(0);
  let message;
  try {
    message = await openpgp.message.read(encrypted);
  } catch (e) {
    message = await openpgp.message.readArmored(encrypted);
  }

  if (!withPassword && !withSessionKey && !certfile) {
    throw new Error('MISSING_ARG');
  }

  if (withPassword) {
    let options = {
      message: message,
      passwords: withPassword
    }
    openpgp.decrypt(options).then( (clearText) => {
      console.log(clearText.data);
    }).catch((e) => {
      console.error(e);
      return process.exit(CANNOT_DECRYPT);
     });
    return;
  }

  if (withSessionKey) {
    let sessionBuf = fs.readFileSync(withSessionKey);
    const sessionKey = {
      data: sessionBuf,
      algorithm: 'aes256'
    };
    let options = {
      message: message,
      sessionKeys: sessionKey
    }
    openpgp.decrypt(options).then(async (clearText) => {
      console.log(clearText);
    }).catch((e) => {
      console.error(e);
      return process.exit(CANNOT_DECRYPT);
    });
    return;
  }
  const buf = fs.readFileSync(certfile);
  let readKey;
  try {
    readKey = await openpgp.key.read(buf);
  } catch(e) {
    readKey = await openpgp.key.readArmored(buf);
  }
  let options = {
    message: message,
    privateKeys: [readKey]
  };

  let verifyKey;
  if (verifyWith) {
    const verifyBuf = fs.readFileSync(verifyWith);
    try {
      verifyKey = await openpgp.key.read(verifyBuf);
    } catch (e) {
      verifyKey =  await openpgp.key.readArmored(verifyBuf);
    }
    options.publicKeys = verifyKey;
  }

  openpgp.decrypt(options).then( async (clearText) => {
    console.log(clearText.data);
    if (verifyOut && clearText.signatures[0].valid) {
      const today = Date.now();
      const signKey = await  verifyKey[0].getSigningKey();
      fs.writeFileSync(verifyOut, today + ' ' + signKey.getFingerprint() + ' ' + verifyKey[0].primaryKey.getFingerprint());
    }
  }).catch((e) => {
    console.error(e);
    return process.exit(CANNOT_DECRYPT);
  });

  if (sessionKeyOut) {
    openpgp.decryptSessionKeys({
      message: message,
      privateKeys: [readKey]
    }).then( (decryptedSessionKeys) => {
      fs.writeFileSync(sessionKeyOut, decryptedSessionKeys[0].data);
    }).catch((e) => {
      console.error(e);
      return process.exit(CANNOT_DECRYPT);
    });;
  }
};

module.exports = decrypt;
