const openpgp = require('openpgp');
const fs = require('fs');
const process = require('process');

const CERT_CANNOT_ENCRYPT = 17;

const encrypt = async (withPassword, signWith, certfile) => {
  const data = fs.readFileSync(0, 'utf-8');
  if (withPassword) {
    let options = {
      message: openpgp.message.fromText(data),
      passwords: withPassword
    }
    openpgp.encrypt(options).then( (ciphertext) => {
      console.log(ciphertext);
    });
    return;
  }
  const buf = fs.readFileSync(certfile);
  let readKey;
  try {
    readKey = await openpgp.key.read(buf);
  } catch(e) {
    readKey = await openpgp.key.readArmored(buf);
  }
  const aeadSupported = await openpgp.key.isAeadSupported([readKey]);
  if (aeadSupported) {
    openpgp.config.aead_protect = true;
  }
  let message = openpgp.message.fromText(data);
  let options = {
    message: message,
    publicKeys: readKey,
    armor: true
  };
  if (signWith) {
    const signBuf = fs.readFileSync(signWith);
    let signKey;
    try {
      signKey = await openpgp.key.read(signBuf);
    } catch(e) {
      signKey =  await openpgp.key.readArmored(signBuf);
    }
    options.privateKeys = signKey[0];
  }

  openpgp.encrypt(options).then( (ciphertext) => {
//    encrypted = ciphertext.message.packets.write(); // get raw encrypted packets as Uint8Array
    console.log(ciphertext);
  }).catch((e) => {
    console.error(e);
    return process.exit(CERT_CANNOT_ENCRYPT);
  });
};

module.exports = encrypt;
