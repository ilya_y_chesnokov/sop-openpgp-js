const openpgp = require('openpgp');
const fs = require('fs');

const sign = async (certfile) => {

  const buf = fs.readFileSync(certfile);
  let readKey;
  try {
    readKey = await openpgp.key.read(buf);
  } catch (e) {
    readKey = await openpgp.key.readArmored(buf)
  }
  const data = fs.readFileSync(0, 'utf-8');

  let options = {
    message: openpgp.message.fromText(data),
    privateKeys: [readKey],
    armor: true,
    detached: true
  };

  openpgp.sign(options).then( async (signed) => {
    console.log(signed);
  });
};

module.exports = sign;
