const openpgp = require('openpgp');
const fs = require('fs');

const extract = async (armor, certfile) => {
    const buf = fs.readFileSync(certfile);
    let readKey;
    try {
      readKey = await openpgp.key.read(buf);
    } catch (e) {
      readKey = await openpgp.key.readArmored(buf);
    }
    const pubKey = readKey.toPublic();
    if (!armor) {
        fs.writeSync(1, pubKey.toPacketlist().write());
        return;
    }
    console.log(pubKey.armor());
}


module.exports = extract;
